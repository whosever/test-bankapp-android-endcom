package com.whoseverstudios.test_bankapp_android_endcom.ldn;

import org.json.JSONObject;

public interface VolleyCallBack {
    void onSuccess(JSONObject json);
}
