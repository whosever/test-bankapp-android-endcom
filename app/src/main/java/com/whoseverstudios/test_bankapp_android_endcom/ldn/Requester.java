package com.whoseverstudios.test_bankapp_android_endcom.ldn;

import android.content.Context;
import android.util.Log;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.Request;
import com.google.gson.JsonObject;

import org.json.JSONObject;

public class Requester {
    private RequestQueue requestQueue;

    public Requester(Context context){
        requestQueue= Volley.newRequestQueue(context);
    }

    public void Request(String url, VolleyCallBack callBack){

        Request<JSONObject> myreq = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        callBack.onSuccess(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(null, "Error Respuesta en JSON: " + error.getMessage());

            }
        });

        requestQueue.add(myreq);

    }
}

