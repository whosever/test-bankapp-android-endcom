package com.whoseverstudios.test_bankapp_android_endcom;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.whoseverstudios.test_bankapp_android_endcom.ldn.Requester;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Iterator;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.myToolbar);
        setSupportActionBar(myToolbar);

        Requester req = new Requester(this);

        // Asumo que todas las validaciones respectivas ya están hechas y solo imprimo los datos

        req.Request("http://bankapp.endcom.mx/api/bankappTest/cuenta", json -> {
            try {
                 JSONArray cuentas = (JSONArray) json.get("cuenta");
                 JSONObject client = (JSONObject)cuentas.get(0);

                ((TextView)findViewById(R.id.costumerName)).setText(client.getString("nombre"));
                ((TextView)findViewById(R.id.lastLogin)).setText(getText(R.string.lastSesion)+ client.getString("ultimaSesion"));

            }catch (Exception e){
                 Toast.makeText(this, e + "", Toast.LENGTH_LONG).show();
            }
        });

        req.Request("http://bankapp.endcom.mx/api/bankappTest/saldos", json ->{
            try {
                JSONArray cuentas = (JSONArray) json.get("saldos");
                JSONObject client = (JSONObject)cuentas.get(0);

                LinearLayout list = (LinearLayout) findViewById(R.id.listToScroll);
                list.removeAllViews();

                LayoutInflater inflater = LayoutInflater.from(this);

                View card = inflater.inflate(R.layout.cuenta,list,false);
                ((TextView)card.findViewById(R.id.accountName)).setText("Saldo General");
                ((TextView)card.findViewById(R.id.numberTexto)).setText(String.format("$%.2f",client.getDouble("saldoGeneral")));

                list.addView(card);

                card = inflater.inflate(R.layout.cuenta,list,false);
                ((TextView)card.findViewById(R.id.accountName)).setText("ingresos");
                ((TextView)card.findViewById(R.id.numberTexto)).setText(String.format("$%.2f",client.getDouble("ingresos")));

                list.addView(card);

                card = inflater.inflate(R.layout.cuenta,list,false);
                ((TextView)card.findViewById(R.id.accountName)).setText("gastos");
                ((TextView)card.findViewById(R.id.numberTexto)).setText(String.format("$%.2f",client.getDouble("gastos")));

                list.addView(card);

            }catch (Exception e){
                Toast.makeText(this, e + "", Toast.LENGTH_LONG).show();
            }
        });

        req.Request("http://bankapp.endcom.mx/api/bankappTest/tarjetas", json ->{
            try {
                JSONArray tarjetas = (JSONArray) json.get("tarjetas");
                LinearLayout list = (LinearLayout)findViewById(R.id.lisTarjetas);
                list.removeAllViews();
                LayoutInflater inflater = LayoutInflater.from(this);

                for(int i = 0; i < tarjetas.length(); i++){
                    JSONObject tarjeta = (JSONObject)tarjetas.get(i);

                    View card = inflater.inflate(R.layout.tarjeta,list,false);
                    ((TextView)card.findViewById(R.id.cardNumber)).setText(tarjeta.getString("tarjeta"));
                    ((TextView)card.findViewById(R.id.cardName)).setText(tarjeta.getString("nombre"));
                    ((TextView)card.findViewById(R.id.cardTitulo)).setText(tarjeta.getString("tipo"));
                    ((TextView)card.findViewById(R.id.cardSaldo)).setText(String.format("$%.2f",tarjeta.getDouble("saldo")));
                    String status = tarjeta.getString("estado");
                    ((TextView)card.findViewById(R.id.cardStatus)).setText(status);
                    if(status.equals("desactivada")){
                        ((ImageView)card.findViewById(R.id.cardimage)).setColorFilter(getColor(R.color.gray));
                    }
                    list.addView(card);
                }
            }catch (Exception e){
                Toast.makeText(this, e + "", Toast.LENGTH_LONG).show();
            }
        });

        req.Request("http://bankapp.endcom.mx/api/bankappTest/movimientos", json ->{
            try {
                JSONArray movimientos = (JSONArray) json.get("movimientos");
                LinearLayout list = (LinearLayout)findViewById(R.id.movementsList);
                list.removeAllViews();
                LayoutInflater inflater = LayoutInflater.from(this);

                for(int i = 0; i < movimientos.length(); i++){
                    JSONObject movimiento = (JSONObject)movimientos.get(i);

                    View card = inflater.inflate(R.layout.movimiento,list,false);
                    ((TextView)card.findViewById(R.id.movDate)).setText(movimiento.getString("fecha"));
                    ((TextView)card.findViewById(R.id.movConcepto)).setText(movimiento.getString("descripcion"));

                    float monto = (float) movimiento.getDouble("monto");
                    String status = movimiento.getString("tipo");
                    if(status.equals("abono")){
                        ((TextView)card.findViewById(R.id.movAmount)).setText(String.format("+$%.2f",monto));
                        ((TextView)card.findViewById(R.id.movAmount)).setTextColor(getColor(R.color.green));
                    }else{
                        ((TextView)card.findViewById(R.id.movAmount)).setText(String.format("-$%.2f",monto));
                        ((TextView)card.findViewById(R.id.movAmount)).setTextColor(getColor(R.color.red));
                    }
                    list.addView(card);
                }
            }catch (Exception e){
                Toast.makeText(this, e + "", Toast.LENGTH_LONG).show();
            }
        });
    }
}